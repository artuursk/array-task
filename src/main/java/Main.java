import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class Main {

    public static void main(String[] args){

        /*

        Implement the following functionalities based on 100,000 element arrays with randomly selected values:
            1. return a list of unique items,
            2. return a list of elements that have been repeated at least once in the generated array,
            3. return a list of the 25 most frequently recurring items.

        Implement a method that deduplicates items in the list. If a duplicate is found, it replaces it with a new random value that did not occur before. Check if the method worked correctly by calling method number 2. (edited)

        */

        //int[] array = new int[10];
        int[] array = {1, 2, 3, 4, 1, 2, 3, 5};
        //System.out.println("Integer max value: " + Integer.MAX_VALUE);

        /*
        for (int i = 0; i < array.length; i++){
            array[i] = getRandomNumber();
            System.out.println(array[i]);
        }*/

        //showUniqueElements(array);
        //showRepeatingElements(array);
        deduplicateElements(array);
    }

    public static void deduplicateElements(int[] array){
        List<Integer> elements = new ArrayList<Integer>();

        for (int i = 0; i < array.length; i++){
            if(!elements.contains(array[i])){
                elements.add(array[i]);
            } else {
                array[i] = getRandomNumber();
            }
        }

        for (int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }

    }

    public static void showRepeatingElements(int[] array){
        Map<Integer, Integer> elements = new HashMap<Integer, Integer>();
        // [1, 2, 3, 1, 2, 2]

        // [1, 2]

        // 2 : 2
        // 1 : 1
        // 3 : 0
        for (int i = 0; i < array.length; i++){

        }
    }

    public static void showUniqueElements(int[] array) {
        Set<Integer> uniqueElements = new HashSet();
        for (int i = 0; i < array.length; i++){
            uniqueElements.add(array[i]);
        }
        System.out.println(uniqueElements);
    }



    public static int getRandomNumber() {
        Random random = new Random();
        int randomNumber = random.nextInt(1000);
        return randomNumber;
    }


}
